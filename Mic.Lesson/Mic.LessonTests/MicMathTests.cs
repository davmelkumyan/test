﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mic.Lesson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Tests
{
    [TestClass()]
    public class MicMathTests
    {
        Random rnd = new Random();

        [TestMethod()]
        public void SortByteTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SortBubbleTest()
        {
            //Arrange
            bool isSorted = false;
            int count = 100;
            int[] actual1 = new int[count];
            int[] actual2 = new int[count];
            int[] actual3 = new int[count];
            int[] actual4 = new int[count];
            int[] actual5 = new int[count];
            int[] expected1 = new int[count];
            int[] expected2 = new int[count];
            int[] expected3 = new int[count];
            int[] expected4 = new int[count];
            int[] expected5 = new int[count];


            //Act
            for (int i = 0; i < count; i++)
            {
                actual1[i] = rnd.Next(100);
                actual2[i] = 0;
                actual3[i] = rnd.Next(-101, 0);
                actual4[i] = rnd.Next(-100, 101);
                actual5[i] = actual1[i];
                expected1[i] = actual1[i];
                expected2[i] = actual2[i];
                expected3[i] = actual3[i];
                expected4[i] = actual4[i];
            }

            for (int i = 0; i < count - 1; i++)
            {
                if (actual5[i] <= actual5[i + 1])
                    isSorted = true;
                else
                {
                    isSorted = false;
                    break;
                }
            }

            MicMath.SortBubble(actual1);
            MicMath.SortBubble(actual2);
            MicMath.SortBubble(actual3);
            MicMath.SortBubble(actual4);
            MicMath.SortBubble(actual5);
            Array.Sort(expected1);
            Array.Sort(expected2);
            Array.Sort(expected3);
            Array.Sort(expected4);


            //Expected
            Assert.IsTrue(actual1.SequenceEqual(expected1));
            Assert.IsTrue(actual2.SequenceEqual(expected2));
            Assert.IsTrue(actual3.SequenceEqual(expected3));
            Assert.IsTrue(actual4.SequenceEqual(expected4));
            Assert.IsTrue(isSorted);
        }

        [TestMethod()]
        public void SortBubbleTest1()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SortBubbleTest2()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SortBubbleTest3()
        {
            //Arrange
            bool isSorted = false;
            int count = 100;
            double[] actual1 = new double[count];
            double[] actual2 = new double[count];
            double[] actual3 = new double[count];
            double[] actual4 = new double[count];
            double[] actual5 = new double[count];
            double[] expected1 = new double[count];
            double[] expected2 = new double[count];
            double[] expected3 = new double[count];
            double[] expected4 = new double[count];
            double[] expected5 = new double[count];


            //Act
            for (int i = 0; i < count; i++)
            {
                actual1[i] = rnd.Next(100);
                actual2[i] = 0;
                actual3[i] = rnd.Next(-101, 0);
                actual4[i] = rnd.Next(-100, 101);
                actual5[i] = actual1[i];
                expected1[i] = actual1[i];
                expected2[i] = actual2[i];
                expected3[i] = actual3[i];
                expected4[i] = actual4[i];
            }

            for (int i = 0; i < count - 1; i++)
            {
                if (actual5[i] <= actual5[i + 1])
                    isSorted = true;
                else
                {
                    isSorted = false;
                    break;
                }
            }

            MicMath.SortBubble(actual1);
            MicMath.SortBubble(actual2);
            MicMath.SortBubble(actual3);
            MicMath.SortBubble(actual4);
            MicMath.SortBubble(actual5);
            Array.Sort(expected1);
            Array.Sort(expected2);
            Array.Sort(expected3);
            Array.Sort(expected4);


            //Expected
            Assert.IsTrue(actual1.SequenceEqual(expected1));
            Assert.IsTrue(actual2.SequenceEqual(expected2));
            Assert.IsTrue(actual3.SequenceEqual(expected3));
            Assert.IsTrue(actual4.SequenceEqual(expected4));
            Assert.IsTrue(isSorted);
        }

        [TestMethod()]
        public void SortBubbleTest4()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SortBubbleTest5()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SortSelectionTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SortSelectionTest1()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SortSelectionTest2()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SortSelectionTest3()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SortSelectionTest4()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SortSelectionTest5()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SortInsertionTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SortInsertionTest1()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SortInsertionTest2()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SortInsertionTest3()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SortInsertionTest4()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SortInsertionTest5()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void IsPrimeTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetAllPrimesTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetAllPrimesTest1()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetAllPrimesTest2()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ShuffleIDTest()
        {
            //Arrange
            int expected = 50;
            int idCount = 125;
            int actual = 0;
            int countNotEqual = 0;
            int[] idShuffled = new int[idCount];
            int[] idNotShuffled = new int[idCount];


            //Act
            for (int i = 0; i < idCount; i++)
            {
                idShuffled[i] = idNotShuffled[i] = i;
            }

            MicMath.ShuffleID(idShuffled);

            for (int i = 0; i < idCount; i++)
            {
                if (idShuffled[i] != idNotShuffled[i])
                    countNotEqual++;
            }
            //one percent of all count
            double onePerc = idCount / 100 + ((idCount % 100) * 0.01); 
            actual = (int)(countNotEqual / onePerc);


            //Expected
            Assert.IsTrue(actual >= expected);
        }

        [TestMethod()]
        public void ChangerTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ChangerTest1()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GroupTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SwapTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SwapTest1()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SwapTest2()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SwapTest3()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SwapTest4()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SwapTest5()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SwapTest6()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetMaxTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetRowMaxTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetRowMaxTest1()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetPillarMaxTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetPillarMaxTest1()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetMinTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetRowMinTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetRowMinTest1()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetPillarMinTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetPillarMinTest1()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetSumTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetSumTest1()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetSumTest2()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void IsEvenTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetAverageTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetAverageTest1()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetAvarageTest()
        {
            Assert.Fail();
        }
    }
}