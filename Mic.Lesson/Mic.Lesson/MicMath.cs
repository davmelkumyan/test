﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson
{
    public class MicMath
    {
        #region Sort

        #region SortByte


        public static void SortByte(byte[] array)
        {
            byte[] counter = new byte[array.Length];
            foreach (byte item in array)
            {
                counter[item]++;
            }
            for (byte i = 0, index = 0; i < counter.Length; i++)
            {
                for (int j = 0; j < counter[i]; j++)
                {
                    array[index] = i;
                    index++;
                }
            }
        }

        #endregion

        #region SortBubble

        //integers
        public static void SortBubble(int[] array)
        {
            for (int i = 0; i < array.Length - 1; i++)
            {
                bool swaped = false;
                for (int j = 0; i < array.Length - i - 1; i++)
                {
                    if (array[j] > array[j + 1])
                    {
                        Swap(array, j, j + 1);
                        swaped = true;
                    }
                }
                if (!swaped)
                    break;
            }
        }

        public static void SortBubble(int[,] matrix)
        {
            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int i = 0; i < matrix.GetLength(1) - 1; i++)
                {
                    bool swaped = false;
                    for (int col = 0; col < matrix.GetLength(1) - col - 1; col++)
                    {
                        if (matrix[row, col] > matrix[row, col + 1])
                        {
                            Swap(matrix, row, col, col + 1);
                            swaped = true;
                        }
                    }
                    if (!swaped)
                        break;
                }
            }
        }

        public static void SortBubble(int[][] jacMatrx, int colCount)
        {
            for (int row = 0; row < jacMatrx.Length; row++)
            {
                for (int j = 0; j < colCount - 1; j++)
                {
                    bool swaped = false;
                    for (int col = 0; col < col - j - 1; col++)
                    {
                        if (jacMatrx[row][col] > jacMatrx[row][col + 1])
                        {
                            Swap(jacMatrx, row, col, col + 1);
                            swaped = true;
                        }
                    }
                    if (!swaped)
                        break;
                }
            }
        }

        //doubles
        public static void SortBubble(double[] array)
        {
            for (int i = 0; i < array.Length - 1; i++)
            {
                bool swaped = false;
                for (int j = 0; j < array.Length - i - 1; j++)
                {
                    if (array[j] > array[j + 1])
                    {
                        Swap(array, j, j + 1);
                        swaped = true;
                    }
                }
                if (!swaped)
                    break;
            }
        }

        public static void SortBubble(double[,] matrix)
        {
            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int i = 0; i < matrix.GetLength(1) - 1; i++)
                {
                    bool swaped = false;
                    for (int col = 0; col < matrix.GetLength(1) - col - 1; col++)
                    {
                        if (matrix[row, col] > matrix[row, col + 1])
                        {
                            Swap(matrix, row, col, col + 1);
                            swaped = true;
                        }
                    }
                    if (!swaped)
                        break;
                }
            }
        }

        public static void SortBubble(double[][] jacMatrx, int colCount)
        {
            for (int row = 0; row < jacMatrx.Length; row++)
            {
                for (int j = 0; j < colCount - 1; j++)
                {
                    bool swaped = false;
                    for (int col = 0; col < col - j - 1; col++)
                    {
                        if (jacMatrx[row][col] > jacMatrx[row][col + 1])
                        {
                            Swap(jacMatrx, row, col, col + 1);
                            swaped = true;
                        }
                    }
                    if (!swaped)
                        break;
                }
            }
        }
        #endregion

        #region SortSelection
        //integers
        public static void SortSelection(int[] array)
        {
            for (int i = 0; i < array.Length - 1; i++)
            {
                int minIndex = i;
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[minIndex] > array[j])
                        minIndex = j;
                }
                if (array[minIndex] != array[i])
                    Swap(array, minIndex, i);
            }
        }

        public static void SortSelection(int[,] matrix)
        {
            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int i = 0; i < matrix.GetLength(1) - 1; i++)
                {
                    int minIndex = i;
                    for (int j = i + 1; j < matrix.GetLength(1); j++)
                    {
                        if (matrix[row, minIndex] > matrix[row, j])
                            minIndex = j;
                    }
                    if (matrix[row, minIndex] != matrix[row, i])
                        Swap(matrix, row, minIndex, i);
                }
            }
        }

        public static void SortSelection(int[][] jacMatrix, int colCount)
        {
            for (int i = 0; i < jacMatrix.Length; i++)
            {
                for (int j = 0; j < colCount - 1; j++)
                {
                    int minIndex = j;
                    for (int k = j + 1; k < colCount; k++)
                    {
                        if (jacMatrix[i][minIndex] > jacMatrix[i][k])
                            minIndex = k;
                    }
                    if (jacMatrix[i][minIndex] != jacMatrix[i][j])
                        Swap(jacMatrix, i, minIndex, j);
                }
            }
        }

        //doubles
        public static void SortSelection(double[] array)
        {
            for (int i = 0; i < array.Length - 1; i++)
            {
                int minIndex = i;
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[minIndex] > array[j])
                        minIndex = j;
                }
                if (array[minIndex] != array[i])
                    Swap(array, minIndex, i);
            }
        }

        public static void SortSelection(double[,] matrix)
        {
            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int i = 0; i < matrix.GetLength(1) - 1; i++)
                {
                    int minIndex = i;
                    for (int j = i + 1; j < matrix.GetLength(1); j++)
                    {
                        if (matrix[row, minIndex] > matrix[row, j])
                            minIndex = j;
                    }
                    if (matrix[row, minIndex] != matrix[row, i])
                        Swap(matrix, row, minIndex, i);
                }
            }
        }

        public static void SortSelection(double[][] jacMatrix, int colCount)
        {
            for (int i = 0; i < jacMatrix.Length; i++)
            {
                for (int j = 0; j < colCount - 1; j++)
                {
                    int minIndex = j;
                    for (int k = j + 1; k < colCount; k++)
                    {
                        if (jacMatrix[i][minIndex] > jacMatrix[i][k])
                            minIndex = k;
                    }
                    if (jacMatrix[i][minIndex] != jacMatrix[i][j])
                        Swap(jacMatrix, i, minIndex, j);
                }
            }
        }


        #endregion

        #region SortInsertion

        public static void SortInsertion(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = i + 1; j > 0; j--)
                {
                    if (array[j - 1] > array[j])
                        Swap(array, j - 1, j);
                }
            }
        }

        public static void SortInsertion(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1) - 1; j++)
                {
                    for (int k = j + 1; k > 0; k--)
                    {
                        if (matrix[i, k - 1] > matrix[i, k])
                            Swap(matrix, i, k - 1, k);
                    }
                }
            }
        }

        public static void SortInsertion(int[][] jacMatrix, int colCount)
        {
            for (int i = 0; i < jacMatrix.Length; i++)
            {
                for (int j = 0; j < colCount - 1; j++)
                {
                    for (int k = j + 1; k > 0; k--)
                    {
                        if (jacMatrix[i][k - 1] > jacMatrix[i][k])
                            Swap(jacMatrix, i, k - 1, k);
                    }
                }
            }
        }


        public static void SortInsertion(double[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = i + 1; j > 0; j--)
                {
                    if (array[j - 1] > array[j])
                        Swap(array, j - 1, j);
                }
            }
        }

        public static void SortInsertion(double[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1) - 1; j++)
                {
                    for (int k = j + 1; k > 0; k--)
                    {
                        if (matrix[i, k - 1] > matrix[i, k])
                            Swap(matrix, i, k - 1, k);
                    }
                }
            }
        }

        public static void SortInsertion(double[][] jacMatrix, int colCount)
        {
            for (int i = 0; i < jacMatrix.Length; i++)
            {
                for (int j = 0; j < colCount - 1; j++)
                {
                    for (int k = j + 1; k > 0; k--)
                    {
                        if (jacMatrix[i][k - 1] > jacMatrix[i][k])
                            Swap(jacMatrix, i, k - 1, k);
                    }
                }
            }
        }

        #endregion

        #region SortFast

        //TODO More Algorithms Add
        #endregion

        #endregion

        #region Prime

        public static bool IsPrime(int value)
        {
            if (value % 2 == 0 || value == 0)
                return false;
            else
            {
                for (int i = 3; i <= Math.Sqrt(value); i += 2)
                {
                    if (value % i == 0)
                        return false;
                }
            }
            return true;
        }

        public static int[] GetAllPrimes(int[] array)
        {
            List<int> lst = array.ToList();
            for (int i = 0; i < lst.Count; i++)
            {
                if (!IsPrime(lst[i]))
                {
                    lst.RemoveAt(i);
                    i--;
                }
            }
            return lst.ToArray();
        }

        public static int[] GetAllPrimes(int[,] array)
        {
            List<int> lst = new List<int>(array.GetLength(0) * array.GetLength(1));
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    lst.Add(array[i, j]);
                }
            }

            for (int i = 0; i < lst.Count; i++)
            {
                if (!IsPrime(lst[i]))
                {
                    lst.RemoveAt(i);
                    i--;
                }
            }
            return lst.ToArray();
        }

        public static int[] GetAllPrimes(int[][] jacMatrix, int colCount)
        {
            List<int> lst = new List<int>(jacMatrix.Length * colCount);

            for (int i = 0; i < jacMatrix.Length; i++)
            {
                for (int j = 0; j < colCount; j++)
                {
                    lst.Add(jacMatrix[i][j]);
                }
            }
            for (int i = 0; i < lst.Count; i++)
            {
                if (!IsPrime(lst[i]))
                    lst.RemoveAt(i);
            }
            return lst.ToArray();
        }
        #endregion

        #region Shuffle

        public static void ShuffleID(int[] ids)
        {
            for (int i = 0; i < ids.Length; i++)
            {
                int rIndex = (i + new Random().Next(ids.Length - i));
                Swap(ids, i, rIndex);
            }
        }

        public static void IDChanger(string[] array, int[] id)
        {
            for (int i = 0; i < id.Length; i++)
            {
                Swap(array, i, id[i]);
            }
        }
        public static void IDChanger(int[] array, int[] id)
        {
            for (int i = 0; i < id.Length; i++)
            {
                Swap(array, i, id[i]);
            }
        }

        #endregion

        #region Grouping Functions

        public static int[][] Group(int[] array, int groupCount)
        {
            int[][] generalGroup = new int[groupCount][];
            int groupLength = array.Length / groupCount;
            int temp = 0;
            int[] group = null;

            for (int i = 0; i < groupCount; i++)
            {
                if (groupLength * groupCount + temp == array.Length)
                    group = new int[groupLength];
                else
                {
                    group = new int[groupLength + 1];
                    temp++;
                }
                generalGroup[i] = group;
            }
            for (int j = 0, item = 0; j < groupCount; j++)
            {
                for (int i = 0; i < groupLength; i++)
                {
                    generalGroup[j][i] = array[item];
                    item++;
                }
            }
            return generalGroup;
        }

        #endregion

        #region Swaping Funcs

        //integers
        public static void Swap(int[] array, int index1, int index2)
        {
            int temp = array[index1];
            array[index1] = array[index2];
            array[index2] = temp;
        }

        public static void Swap(int[,] array, int row, int col1, int col2)
        {
            int temp = array[row, col1];
            array[row, col1] = array[row, col2];
            array[row, col2] = temp;
        }

        public static void Swap(int[][] array, int row, int col1, int col2)
        {
            int temp = array[row][col1];
            array[row][col1] = array[row][col2];
            array[row][col2] = temp;
        }

        //doubles
        public static void Swap(double[] array, int index1, int index2)
        {
            double temp = array[index1];
            array[index1] = array[index2];
            array[index2] = temp;
        }

        public static void Swap(double[,] array, int row, int col1, int col2)
        {
            double temp = array[row, col1];
            array[row, col1] = array[row, col2];
            array[row, col2] = temp;
        }

        public static void Swap(double[][] array, int row, int col1, int col2)
        {
            double temp = array[row][col1];
            array[row][col1] = array[row][col2];
            array[row][col2] = temp;
        }

        //strings
        public static void Swap(string[] array, int index1, int index2)
        {
            string temp = array[index1];
            array[index1] = array[index2];
            array[index2] = temp;
        }
        #endregion

        #region Get Max and Min

        //Max
        /// <summary>
        /// Gets Max of Array
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static int GetMax(int[] array)
        {
            int max = -1;
            for (int i = 0; i < array.Length; i++)
                if (max < array[i])
                    max = array[i];
            return max;
        }

        /// <summary>
        /// Get Max of Row in Matix
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        public static int[] GetRowMax(int[,] matrix)
        {
            int[] getMax = new int[matrix.GetLength(0)];
            int max = -1;
            for (int j = 0; j < matrix.GetLength(0); j++)
            {
                for (int i = 0; i < matrix.GetLength(1); i++)
                {
                    if (max < matrix[j, i])
                    {
                        max = matrix[j, i];
                    }
                }
                getMax[j] = max;
                max = -1;
            }

            return getMax;
        }

        /// <summary>
        /// Get Max of Row in Jagged Matix
        /// </summary>
        /// <param name="jMatrix"></param>
        /// <returns></returns>
        public static int[] GetRowMax(int[][] jMatrix, int col)
        {
            int[] getMax = new int[jMatrix.GetLength(0)];
            int max = -1;
            for (int j = 0; j < jMatrix.GetLength(0); j++)
            {
                for (int i = 0; i < col; i++)
                {
                    if (max < jMatrix[j][i])
                    {
                        max = jMatrix[j][i];
                    }
                }
                getMax[j] = max;
                max = -1;
            }

            return getMax;
        }

        /// <summary>
        /// Gets Pillar Max of Matrix
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public static int[] GetPillarMax(int[,] matrix)
        {
            int max = -1;
            int[] getMax = new int[matrix.GetLength(1)];
            for (int i = 0; i < matrix.GetLength(1); i++)
            {
                for (int j = 0; j < matrix.GetLength(0); j++)
                {
                    if (max < matrix[j, i])
                        max = matrix[j, i];
                }
                getMax[i] = max;
                max = -1;
            }
            return getMax;
        }

        /// <summary>
        /// Gets Pillar Max of Jagged Matrix
        /// </summary>
        /// <param name="jMatrix"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public static int[] GetPillarMax(int[][] jMatrix, int col)
        {
            int max = -1;
            int[] getMax = new int[col];
            for (int i = 0; i < col; i++)
            {
                for (int j = 0; j < jMatrix.GetLength(0); j++)
                {
                    if (max < jMatrix[j][i])
                        max = jMatrix[j][i];
                }
                getMax[i] = max;
                max = -1;
            }
            return getMax;
        }

        //Min
        public static int GetMin(int[] array)
        {
            int min = array[0];
            for (int i = 1; i < array.Length; i++)
            {
                if (min > array[i])
                {
                    min = array[i];
                }
            }
            return min;
        }

        /// <summary>
        /// Get Min of Row in Matix
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        public static int[] GetRowMin(int[,] matrix)
        {
            int[] getMax = new int[matrix.GetLength(0)];
            int max = -1;
            for (int j = 0; j < matrix.GetLength(0); j++)
            {
                for (int i = 0; i < matrix.GetLength(1); i++)
                {
                    if (max > matrix[j, i])
                    {
                        max = matrix[j, i];
                    }
                }
                getMax[j] = max;
                max = -1;
            }

            return getMax;
        }

        /// <summary>
        /// Get Min of Row in Jagged Matix
        /// </summary>
        /// <param name="jMatrix"></param>
        /// <returns></returns>
        public static int[] GetRowMin(int[][] jMatrix, int col)
        {
            int[] getMax = new int[jMatrix.GetLength(0)];
            int max = -1;
            for (int j = 0; j < jMatrix.GetLength(0); j++)
            {
                for (int i = 0; i < col; i++)
                {
                    if (max > jMatrix[j][i])
                    {
                        max = jMatrix[j][i];
                    }
                }
                getMax[j] = max;
                max = -1;
            }

            return getMax;
        }

        /// <summary>
        /// Gets Pillar Min of Matrix
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public static int[] GetPillarMin(int[,] matrix)
        {
            int max = -1;
            int[] getMax = new int[matrix.GetLength(1)];
            for (int i = 0; i < matrix.GetLength(1); i++)
            {
                for (int j = 0; j < matrix.GetLength(0); j++)
                {
                    if (max > matrix[j, i])
                        max = matrix[j, i];
                }
                getMax[i] = max;
                max = -1;
            }
            return getMax;
        }

        /// <summary>
        /// Gets Pillar Min of Jagged Matrix
        /// </summary>
        /// <param name="jMatrix"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public static int[] GetPillarMin(int[][] jMatrix, int col)
        {
            int max = -1;
            int[] getMax = new int[col];
            for (int i = 0; i < col; i++)
            {
                for (int j = 0; j < jMatrix.GetLength(0); j++)
                {
                    if (max > jMatrix[j][i])
                        max = jMatrix[j][i];
                }
                getMax[i] = max;
                max = -1;
            }
            return getMax;
        }

        #endregion

        #region Get Sum

        /// <summary>
        /// Get sum of array
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static int GetSum(int[] array)
        {
            int sum = 0;
            foreach (var item in array)
            {
                sum += item;
            }
            return sum;
        }

        /// <summary>
        /// Gets every Matrix row sum and set to array
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        public static int[] GetSum(int[,] matrix)
        {
            int[] getSum = new int[matrix.GetLength(0)];
            int sum = 0;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    sum += matrix[i, j];
                }
                getSum[i] = sum;
                sum = 0;
            }
            return getSum;
        }

        /// <summary>
        /// Gets every Jagged Matrix row sum and set to array
        /// </summary>
        /// <param name="jMatrix"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public static int[] GetSum(int[][] jMatrix, int col)
        {
            int[] getSum = new int[jMatrix.GetLength(0)];
            int sum = 0;
            for (int i = 0; i < jMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < col; j++)
                {
                    sum += jMatrix[i][j];
                }
                getSum[i] = sum;
                sum = 0;
            }
            return getSum;
        }

        #endregion

        #region Is Even or Odd

        //Return true if num is even false if is odd
        public static bool IsEven(int num)
        {
            if (num % 2 == 0)
            {
                return true;
            }
            return false;
        }

        #endregion

        #region IsPerfect

        //Get true if is Perfect else false
        static bool IsPerfect(int num)
        {
            int sum = 0;

            for (int i = 1; i < num; i++)
                if (num % i == 0)
                {
                    sum += i;
                }
            if (sum == num)
                return true;
            return false;
        }

        #endregion

        #region Get Max Element of Num

        // Geting Max Element of number
        static int GetMaxElement(int num)
        {
            int max = 0;
            while (num > 0)
            {
                int el = num % 10;
                num /= 10;
                if (el > max)
                {
                    max = el;
                }
            }
            return max;
        }

        #endregion

        #region Get Avarage

        //return average of array numbers
        public static double GetAverage(int[] array)
        {
            double sum = 0d;
            int count = 0;
            for (int i = 0; i < array.Length; i++)
            {
                count++;
                sum += array[i];
            }
            return sum / count;
        }

        public static double GetAverage(int[,] array)
        {
            double sum = 0d;
            int count = 0;
            for (int j = 0; j < array.GetLength(0); j++)
            {
                for (int i = 0; i < array.GetLength(1); i++)
                {
                    count++;
                    sum += array[j, i];
                }
            }
            return sum / count;
        }

        public static double GetAvarage(int[][] array, int colCount)
        {
            double sum = 0d;
            int count = 0;
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < colCount; j++)
                {
                    count++;
                    sum += array[i][j];
                }
            }
            return sum / count;
        }

        #endregion

    }
}

